const mongoose = require('mongoose');
const schemas = require('../models');

require('dotenv').config();

const connection = {};

// const options = { useMongoClient: true, keepAlive: 300000, connectTimeoutMS: 30000 };
const options = {
  keepAlive: true, connectTimeoutMS: 30000, socketTimeoutMS: 0,
  // useNewUrlParser: true,
  // replicaSet: 'Eyowo-shard-0',
};
// connection.db = mongoose.createConnection('mongodb://cpc:password123456789@npvn-shard-00-00-arxd6.mongodb.net:27017,npvn-shard-00-01-arxd6.mongodb.net:27017,npvn-shard-00-02-arxd6.mongodb.net:27017/npvn?ssl=true&replicaSet=NPVN-shard-0&authSource=admin', options);

// connection.db = mongoose.createConnection(process.env.DB_HOST, options, (err) => {
connection.db = mongoose.createConnection(`mongodb://${process.env.MONGO_USER}:${process.env.MONGO_USER_PASSWORD}@eyowo-shard-00-00-wswtr.mongodb.net:27017,eyowo-shard-00-01-wswtr.mongodb.net:27017,eyowo-shard-00-02-wswtr.mongodb.net:27017/eyowo?ssl=true&replicaSet=Eyowo-shard-0&authSource=admin`, options, (err) => {
  if (err) throw err;
  console.log('db connected');
});
connection.models = {};
connection.models.user = connection.db.model('User', schemas.UserSchema);

module.exports = connection;
