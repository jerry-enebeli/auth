
const mongoose = require('mongoose');


const cloudinary = require('cloudinary');

cloudinary.config({
  cloud_name: 'softcomdevs',
  api_key: '577134857352314',
  api_secret: '4HTrHLJZ5mXCiTvjmNJ_DVS11jw',
});

exports.singleUpload = function singleUpload(file) {
  if (!file) {
    return 'no file uploaded';
  }
  console.log(file);
  const entryimage = file.image;
  const imageId = new mongoose.mongo.ObjectID();
  const extension = entryimage.name.split('.').pop();
  return new Promise((resolve, reject) => {
    entryimage.mv(`/tmp/${imageId}.${extension}`, () => {
      cloudinary.uploader.upload(`/tmp/${imageId}.${extension}`, (result) => {
        console.log(result);
        resolve(result.secure_url);
      }, {
        public_id: imageId,
      });
    });
    // return `http://res.cloudinary.com/softcomdevs/image/upload/${imageId}.${extension}`;
  });
};
