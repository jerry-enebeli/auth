const grpc = require('grpc');
const protoLoader = require('@grpc/proto-loader');
const redis = require('redis');
const rmdstr = require('randomstring');
const jwt = require('jsonwebtoken');
const conn = require('./config/db');
const sms  = require('./util/message');
const redisUtil = require('./util/redis.js');

require('dotenv').config();

const PROTO_PATH = `${__dirname}/proto/auth.proto`;

const packageDefinition = protoLoader.loadSync(
  PROTO_PATH,
  {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true,
  },
);

// const AuthService = grpc.load(PROTO_PATH).eyowo_auth;
const AuthService = grpc.loadPackageDefinition(packageDefinition).eyowo_auth;

const User = conn.models.user;

const redisClient = redis.createClient(process.env.REDIS_HOST); // creates a new client

redisClient.on('connect', () => {
  console.log('Connected to Redis!');
});

function generateToken() {
  const token = rmdstr.generate({
    length: 6,
    charset: '0123456789',
  });
  return token;
}

function testMessage(call, callback) {
  callback(null, { success: true, data: `Hello ${call.request.mobile}` });
}

function startValidation(mobile) {
  const token = generateToken();

  // send SMS to mobile number
  sms.sms(JSON.stringify({
    mobile,
    message: `Your Eyowo validation code is: ${token}`,
    sender: 'Eyowo',
  }));
  // store token with mobile number for future validation
  const key = redisUtil.createRedisKey(`SMS_VALIDATE%${mobile}`);
  const value = `${token}`;
  redisClient.set(key, value);
  // 2nd parameter is in seconds. We set it to expire in 30mins
  redisClient.expire(key, 1800);
}

async function validateMobile(call, callback) {
  const { mobile } = call.request;

  try {
    const user = await User.findOne({ mobile });

    if (user) {
      // If user has a secure PIN set, used that to authenticate instead
      if (user.secure_pin) {
        callback(null, {
          success: true,
          secure_pin: true,
          message: 'Eyowo Secure PIN validation started successfully',
        });
      } else {
        startValidation(mobile);
        callback(null, {
          success: true,
          secure_pin: false,
          message: 'Eyowo SMS token validation started successfully',
        });
      }
    }
    if (!user) {
      startValidation(mobile);
      callback(null, {
        success: true,
        secure_pin: false,
        message: 'Eyowo SMS token validation started successfully',
      });
    }
  } catch (err) {
    console.error(err);
    callback(err);
  }
}

async function validateSecurePIN(call, callback) {
  // callback(null, { success: true, data: `Hello again, ${call.request.mobile}` });
  const { mobile } = call.request;
  const { secure_pin } = call.request;
  console.log(call.request);
  try {
    const user = await User.findOne({
      mobile,
    });

    console.log(user);

    if (!user) {
      // No user found
      callback(null, {
        success: false,
        message: 'Eyowo Secure PIN authentication failed',
      });
      return;
    }

    if (user.compareSecurePin(secure_pin)) {
      const token = jwt.sign({
        _id: user._id.toString(),
        mobile: user.mobile,
      }, process.env.TOKENSECRET, {
        expiresIn: 1440 * 60, // expires in 24 hours //input is in seconds
      });

      const userObj = user.toJSON();
      delete userObj.secure_pin;
      // delete userObj._id;

      callback(null, {
        success: true,
        user: JSON.stringify(userObj),
        token,
        message: 'User validated successfully',
        _id: user._id.toString(),
      });
      return;
    }
    callback(null, {
      success: false,
      message: 'Invalid Eyowo Secure PIN provided',
    });
    return;
  } catch (err) {
    console.error(err);
    callback(err);
  }
}

async function validateAuthorizationHeader(call, callback) {
  // callback(null, { success: true, data: `Hello again, ${call.request.mobile}` });
  const { token } = call.request;

  // decode token
  // verifies secret and checks exp
  jwt.verify(token, process.env.TOKENSECRET, (err, decoded) => {
    if (err) {
      console.error(err);
      callback(null, {
        success: false,
        message: 'Failed to authenticate token.',
        status: 500,
      });
    }
    User.findById(decoded._id, (errr, user) => {
      if (errr) {
        console.log('nothing found');
        console.error(errr);
        callback(null, {
          success: false,
          message: 'Authentication failed. Eyowo user not found.',
          status: 403,
        });
      }
      // if everything is good, save to request for use in other routes
      callback(null, {
        success: true,
        message: 'Token validated successfully',
        decoded: JSON.stringify(decoded),
        user: JSON.stringify(user),
        status: 200,
      });
    });
  });
}

async function reauthorizeToken(call, callback) {
  const { previous_token } = call.request;
  const { mobile } = call.request;

  jwt.verify(previous_token, process.env.TOKENSECRET, (err, decoded) => {
    console.log(err);
    console.log(decoded);
    if (err !== null && err.name !== null && err.name !== 'TokenExpiredError') {
      callback(null, {
        success: false,
        message: 'Failed to authenticate token.',
        err,
        status: 500,
      });
      return;
    }
    User.findById(decoded._id, (errr, user) => {
      if (errr) {
        console.log('nothing found');
        callback(null, {
          success: false,
          message: 'Authentication failed. Eyowo user not found.',
          status: 403,
        });
        return;
      }
      if (user.mobile !== mobile) {
        callback(null, {
          success: false,
          message: 'Authentication failed. Operation not allowed.',
          status: 403,
        });
        return;
      }

      const token = jwt.sign({
        _id: user._id.toString(),
        mobile: user.mobile,
      }, process.env.TOKENSECRET, {
        expiresIn: 1440 * 60 * 30, // expires in 30 days //input is in seconds
      });

      callback(null, {
        success: true,
        token,
        message: 'User validated successfully',
        _id: user._id,
        status: 200,
      });
    });
  });
}


async function validateSms(call, callback) {
  const req = call.request;
  User.findOne({
    mobile: req.mobile,
  }, (err, user) => {
    if (err) {
      console.log(err);
    }
    if (user) {
      // If user has a secure PIN set, used that to authenticate instead
      if (user.secure_pin) {
        callback(null, {
          success: true,
          secure_pin: true,
          message: 'Eyowo Secure PIN validation started successfully',
        });
      } else {
        startValidation(req.mobile);
        callback(null, {
          success: true,
          secure_pin: false,
          message: 'Eyowo SMS token validation started successfully',
        });
      }
    }
    if (!user) {
      startValidation(req.mobile);
      callback(null, {
        success: true,
        secure_pin: false,
        message: 'Eyowo SMS token validation started successfully',
      });
    }
  });
}

function main() {
  const server = new grpc.Server();
  server.addService(
    AuthService.Auth.service,
    {
      testMessage, validateMobile, validateSecurePIN, validateAuthorizationHeader, reauthorizeToken, validateSms,
    },
  );
  server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure());
  server.start();

  console.log('Eyowo Auth Service Started!!!');
}

main();
